# KTH salsschema

__Appen är inte under aktiv utveckling.__

Android app som gör slagningar mot http://www.kth.se/schema på sals basis.

Kan hittas på Google Play https://play.google.com/store/apps/details?id=com.snilius.salschema.

Appen är inte under aktiv utveckling, patchar är dock välkomna.

## License

    KTH salsschema
    Copyright (C) 2014-2016 Victor Häggqvist

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.