package com.snilius.salsschema;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.jakewharton.disklrucache.DiskLruCache;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.koushikdutta.ion.Response;

import net.fortuna.ical4j.data.CalendarBuilder;
import net.fortuna.ical4j.data.ParserException;
import net.fortuna.ical4j.model.Calendar;
import net.fortuna.ical4j.model.Component;
import net.fortuna.ical4j.model.ComponentList;

import java.io.IOException;
import java.io.StringReader;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

import timber.log.Timber;


public class MainActivity extends ActionBarActivity implements TextView.OnEditorActionListener {

    // TODO make input autocompleted since not all rooms are supported (apperently...)
    // TODO ability to "star" rooms

    public static final String ISO8601DATE = "yyyyMMdd'T'HHmmss'Z'";
    private TextView textView;
    private EditText editText;
    private DiskLruCache mCache;
    private String urlKey;
    private LinearLayout mLoadview;
    private int mYear;
    private int mMonth;
    private int mDay;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mCache = ((Application) getApplication()).getCache();

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        mLoadview = (LinearLayout) findViewById(R.id.loadingView);

        textView = (TextView) findViewById(R.id.text);
        editText = (EditText) findViewById(R.id.input);

        // on enter listener
        editText.setOnEditorActionListener(this);

        final java.util.Calendar c = java.util.Calendar.getInstance();
        mYear = c.get(java.util.Calendar.YEAR);
        mMonth = c.get(java.util.Calendar.MONTH);
        mDay = c.get(java.util.Calendar.DAY_OF_MONTH);
    }

    private void loadUrl(String url) {
        if (BuildConfig.DEBUG)
            Ion.getDefault(this).configure().setLogging("Ion", Log.DEBUG);

        Ion.with(this).load(url)
                .asString()
                .withResponse()
                .setCallback(new ICalCallback());
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        switch (id){
            case R.id.action_about:
                startActivity(new Intent(this, AboutActivity.class));
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    private void fetchTimeTable(String input) {
        if (input.length()<1)
            return;
        mLoadview.setVisibility(View.VISIBLE);
        java.util.Calendar cal = java.util.Calendar.getInstance();
        cal.set(mYear, mMonth, mDay);
        Date date = cal.getTime();
        System.out.println(date);
        String formatted = new SimpleDateFormat("yyyy-MM-dd").format(date);
        String query = null;
        try {
            query = URLEncoder.encode(input, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        String url = "http://www.kth.se/schema/ical?showweekend=false&start="+formatted+"&end="+formatted+"&freetext=&location="+query+"&publishState=";
        Timber.i(url);
        urlKey = Integer.toString(url.hashCode());
        try {
            DiskLruCache.Snapshot snapshot = mCache.get(urlKey);
            String cache = snapshot.getString(0);
            Timber.i("using cache");
            parseEvent(cache);
        } catch (Exception e) {
            Timber.i("no cache");
            loadUrl(url);
        }
    }

    @Override
    public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
        fetchTimeTable(editText.getText().toString());
        return false;
    }

    public void pickDate(View view) {
        DatePickerFragment dialogFragment = new DatePickerFragment();
        dialogFragment.setInitState(this, mDay, mMonth, mYear);
        dialogFragment.show(getFragmentManager(), "datePicker");
    }

    private class ICalCallback implements FutureCallback<Response<String>> {
        @Override
        public void onCompleted(Exception e, Response<String> result) {
            if (null == result) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(getApplication(), getString(R.string.fetchfail), Toast.LENGTH_LONG).show();
                    }
                });
            } else {
                if (result.getHeaders().code() == 200)
                    parseEvent(result.getResult());
                else
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(getApplication(), getString(R.string.fetchfail), Toast.LENGTH_LONG).show();
                        }
                    });
            }
        }
    }

    /**
     * Parse iCal string, create calendar
     * @param result raw iCal data
     */
    private void parseEvent(String result) {
        if (result == null)
            return;

        Timber.i("parsing");

        StringReader sr = new StringReader(result);
        CalendarBuilder builder = new CalendarBuilder();

        Calendar cal = null;
        try {
            cal = builder.build(sr);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ParserException e) {
            e.printStackTrace();
        }

        renderEvents(cal);
        try {
            DiskLruCache.Editor edit = mCache.edit(urlKey);
            edit.set(0, result);
            edit.commit();
            Timber.i(urlKey+" added to cache");
        } catch (IOException e) {
            Timber.e("Failed to put cache");
            e.printStackTrace();
        }
    }

    /**
     * Render events in a Calendar
     * @param cal ical4j.model.Calendar object
     */
    private void renderEvents(Calendar cal) {
        if (cal.getComponents().size() < 1){
            mLoadview.setVisibility(View.GONE);
            textView.setText(getString(R.string.no_events));
            return;
        }

        Timber.i("rendering");

        ComponentList list = cal.getComponents();
        StringBuilder sb = new StringBuilder();
        for (Object listObject:list){
            Component c = (Component) listObject;
            sb.append(getComonentProp(c, "summary"));

            String startTime = parseDate(getComonentProp(c, "dtstart"));
            sb.append("\n"+getString(R.string.start)+startTime);
            String endTime = parseDate(getComonentProp(c, "dtend"));
            sb.append("\n"+getString(R.string.end)+endTime);
            sb.append("\n");
        }

        mLoadview.setVisibility(View.GONE);
        textView.setText(sb.toString());
    }

    /**
     * Parse date, get time
     * @param date ISO8601 date string
     * @return HH:mm
     */
    private String parseDate(String date) {
        try {SimpleDateFormat sdfIn = new SimpleDateFormat(ISO8601DATE);
            sdfIn.setTimeZone(TimeZone.getTimeZone("GMT"));
            Date dateStart = sdfIn.parse(date);
//            Timber.i(date);
//            Timber.i(dateStart.toString());
            SimpleDateFormat sdfOut = new SimpleDateFormat("HH:mm");
            sdfOut.setTimeZone(TimeZone.getTimeZone("GMT+1"));
            return sdfOut.format(dateStart);
        } catch (ParseException e) {
            return date;
        }
    }

    /**
     * Extract component props
     * @param c The component
     * @param prop Prop to get
     * @return fetched and sanitized prop
     */
    private String getComonentProp(Component c, String prop) {
        String[] parts = c.getProperty(prop.toUpperCase()).getValue().split("\\n");

        if (parts.length == 1)
            return parts[0];
        else{
            StringBuilder sb = new StringBuilder();
            for (String part:parts){
                if (part.length()>0)
                    sb.append("\n"+part);
            }
            return sb.toString();
        }
    }

    protected void setDate(int y, int m, int d){
        mYear = y;
        mMonth = m;
        mDay = d;
    }

    public static class DatePickerFragment extends DialogFragment implements DatePickerDialog.OnDateSetListener {

        private MainActivity activity;
        private int mDay;
        private int mMonth;
        private int mYear;

        public void setInitState(MainActivity activity, int mDay, int mMonth, int mYear) {
            this.activity = activity;
            this.mDay = mDay;
            this.mMonth = mMonth;
            this.mYear = mYear;
        }

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            // Create a new instance of DatePickerDialog and return it
            return new DatePickerDialog(getActivity(), this, mYear, mMonth, mDay);
        }

        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
            activity.setDate(year, monthOfYear, dayOfMonth);
        }
    }
}
