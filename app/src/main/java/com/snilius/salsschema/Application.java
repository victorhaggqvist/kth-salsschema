package com.snilius.salsschema;

import com.crashlytics.android.Crashlytics;
import com.jakewharton.disklrucache.DiskLruCache;

import java.io.IOException;

import io.fabric.sdk.android.Fabric;
import timber.log.Timber;

/**
 * @author victor
 * @since 10/16/14
 */
public class Application extends android.app.Application {

    private DiskLruCache mCache;

    @Override
    public void onCreate() {
        super.onCreate();
        Fabric.with(this, new Crashlytics.Builder().disabled(BuildConfig.DEBUG).build());

        if (BuildConfig.DEBUG)
            Timber.plant(new Timber.DebugTree());

        try {
            mCache = DiskLruCache.open(getFilesDir(), BuildConfig.VERSION_CODE, 1, 200000);
        } catch (IOException e) {
            Timber.d("Faild to open cache");
        }
    }

    public DiskLruCache getCache() {
        return mCache;
    }
}
